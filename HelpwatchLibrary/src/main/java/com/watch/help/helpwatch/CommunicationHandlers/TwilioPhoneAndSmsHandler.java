package com.watch.help.helpwatch.CommunicationHandlers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.watch.help.helpwatch.DataReporting.DataPush.DataRequestSingleton;
import com.watch.help.helpwatch.TwilioHandlers.TwilioDialActivity;
import com.watch.help.helpwatch.TwilioHandlers.TwilioTextRequests;

import static com.watch.help.helpwatch.BuildConfig.HELPWATCH_SERVER_URL;

class TwilioPhoneAndSmsHandler extends BasePhoneAndSmsHandler {

    private final TwilioTextRequests mTwilioTextRequests;
    private String mTwilioToken;
    private Context mParentContext;
    private String mWatchId;

    TwilioPhoneAndSmsHandler(Context context, String watchId){
        mParentContext = context;
        mWatchId = watchId;
        requestTwilioToken();
        mTwilioTextRequests = new TwilioTextRequests(context, mWatchId);

    }

    @Override
    public void sendEmergencyTexts() {
        mTwilioTextRequests.sendEmergencyTexts();
    }

    @Override
    public void sendFinalTexts() {
        //This is unnecessary for Twilio.
    }

    @Override
    public void sendCancellations() {
        mTwilioTextRequests.sendCancelledEmergencyTexts();
    }

    @Override
    public void makeEmergencyCalls() {
        Intent intent = new Intent(mParentContext, TwilioDialActivity.class);
        intent.putExtra("watchId", mWatchId);
        intent.putExtra("twilioToken", mTwilioToken);
        mParentContext.startActivity(intent);
    }

    private void requestTwilioToken() {
        StringRequest request = new StringRequest(Request.Method.POST, HELPWATCH_SERVER_URL + "/accessToken",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VoiceActivity", response);
                        mTwilioToken = response;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", error.toString());
                    }
                }
        );
        DataRequestSingleton.getInstance(mParentContext).addToRequestQueue(request);
    }
}
