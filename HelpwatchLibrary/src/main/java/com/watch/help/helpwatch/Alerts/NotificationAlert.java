package com.watch.help.helpwatch.Alerts;

import android.content.Context;

import com.watch.help.helpwatch.R;

public class NotificationAlert extends BaseAlert {

    public NotificationAlert(Context context) {
        super(context);
        this.mAlarmSound = R.raw.notification_alarm;
        this.mVibrationPattern = new long[] {1000, 9000};
    }
}
