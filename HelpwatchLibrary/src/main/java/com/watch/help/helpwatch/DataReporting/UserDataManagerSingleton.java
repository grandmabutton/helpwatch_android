package com.watch.help.helpwatch.DataReporting;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.watch.help.helpwatch.DataReporting.DataPush.DataPushWorkManager;
import com.watch.help.helpwatch.Helpers.PermissionRequestActivity;
import java.util.concurrent.TimeUnit;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class UserDataManagerSingleton {

    private static final int DEFAULT_PUSH_PERIOD_IN_MINUTES = 15;
    private static final int EMERGENCY_PUSH_PERIOD_IN_SECONDS = 30;
    private static final String EMERGENCY_WORK_TAG = "EmergencyPush";
    private static UserDataManagerSingleton instance;
    private static final String mDebugTag = "USER_DATA_MANAGER";
    private AccelerationListener mAccelerationListener;
    private DeviceIsWornListener mDeviceIsWornListener;
    private GpsHandler mGpsHandler;
    private HeartRateListener mHeartRateListener;
    private StepCountListener mStepCountListener;
    private static final String mUserId = "JUMP_MAN";
    private UserDataContainer mUserDataContainer;
    PeriodicWorkRequest mEmergencyDataPushWork;

    private UserDataManagerSingleton(Context context) {
        mUserDataContainer = new UserDataContainer(mUserId);
        bindBodySensors(context.getApplicationContext());
        bindGps(context.getApplicationContext());
        startDataReporting();
    }

    public static UserDataManagerSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new UserDataManagerSingleton(context.getApplicationContext());
        }
        return instance;
    }

    private void startDataReporting() {
        PeriodicWorkRequest.Builder userDataPushBuilder =
                new PeriodicWorkRequest.Builder(DataPushWorkManager.class,
                        DEFAULT_PUSH_PERIOD_IN_MINUTES,
                        TimeUnit.MINUTES);
        PeriodicWorkRequest dataPushWork = userDataPushBuilder.build();
        WorkManager.getInstance().enqueue(dataPushWork);

    }

    public void oneTimePush(){
        OneTimeWorkRequest pushRequest = new OneTimeWorkRequest.Builder(DataPushWorkManager.class)
                .build();
        WorkManager.getInstance().enqueue(pushRequest);
    }

    private void bindBodySensors(Context context) {
        Intent myIntent = new Intent(context, PermissionRequestActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        myIntent.putExtra("KEY_PERMISSIONS", Manifest.permission.BODY_SENSORS);
        context.startActivity(myIntent);

        mAccelerationListener = new AccelerationListener(context);
        mDeviceIsWornListener = new DeviceIsWornListener(context);
        mHeartRateListener = new HeartRateListener(context);
        mStepCountListener = new StepCountListener(context);
    }

    private void bindGps(Context context){
        Intent myIntent = new Intent(context, PermissionRequestActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        myIntent.putExtra("KEY_PERMISSIONS", Manifest.permission.ACCESS_FINE_LOCATION);
        context.startActivity(myIntent);
        mGpsHandler = new GpsHandler(context);
    }

    public void updateUserData(String dataField, Object value){
        mUserDataContainer.updateUserData(dataField, value);
    }

    public Object getUserData(String dataField){
        return mUserDataContainer.getUserData(dataField);
    }

    public void startEmergencyReporting(){
        Log.d(mDebugTag,"Begin emergency reporting.");
        mHeartRateListener.initiateEmergencyOperation();
        mGpsHandler.launchEmergencyGps();
        PeriodicWorkRequest.Builder emergencyDataPushBuilder =
                new PeriodicWorkRequest.Builder(DataPushWorkManager.class,
                        EMERGENCY_PUSH_PERIOD_IN_SECONDS,
                        TimeUnit.SECONDS);
        mEmergencyDataPushWork = emergencyDataPushBuilder.addTag(EMERGENCY_WORK_TAG).build();
        WorkManager.getInstance().enqueue(mEmergencyDataPushWork);
    }

    public void cancelEmergencyReporting(){
        Log.d(mDebugTag,"Cancel emergency reporting.");
        mHeartRateListener.initiateNormalOperation();
        mGpsHandler.restoreBackgroundGps();
        WorkManager.getInstance().cancelAllWorkByTag(EMERGENCY_WORK_TAG);
    }

    void pauseDataCollection() {
        mAccelerationListener.pause();
        mHeartRateListener.pause();
        mStepCountListener.pause();
        mGpsHandler.pause();
    }

    void resumeDataCollection() {
        mAccelerationListener.initiatNormalOperation();
        mHeartRateListener.initiateNormalOperation();
        mStepCountListener.initiateNormalOperation();
        mGpsHandler.restoreBackgroundGps();
    }
}