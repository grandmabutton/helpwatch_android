package com.watch.help.helpwatch.DataReporting;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class AccelerationListener implements SensorEventListener {

    public static final int MICRO_SECONDS_PER_SECOND = 1000000;
    private final Context mContext;
    private final static String mDebugTag = "ACCELEROMETER";
    private final SensorManager mSensorManager;
    private final Sensor mAccelerometer;

    AccelerationListener(Context context){
        mContext = context.getApplicationContext();
        mSensorManager = (SensorManager)mContext.getSystemService(Context.SENSOR_SERVICE);
        assert mSensorManager != null;
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        boolean isRegistered = mSensorManager.registerListener(this, mAccelerometer,
                10*MICRO_SECONDS_PER_SECOND);
        if (!isRegistered) {
            Log.d(mDebugTag, "Registered? !@#$%^&*NOPE!@#$%^");
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float acceleration = event.values[2];//z component 0 and 1 contain x and y acceleration.
        UserDataManagerSingleton dataManager = UserDataManagerSingleton.getInstance(mContext);
        dataManager.updateUserData("Acceleration",acceleration);
        //Log.d(mDebugTag,"Acceleration measured.");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    void pause() {
        mSensorManager.unregisterListener(this,mAccelerometer);
        Log.d(mDebugTag, "Sensor paused.");
    }

    void initiatNormalOperation() {
        boolean isRegistered = mSensorManager.registerListener(this, mAccelerometer,
                10*MICRO_SECONDS_PER_SECOND);
        if (!isRegistered) {
            Log.d(mDebugTag, "Registered? !@#$%^&*NOPE!@#$%^");
        }
    }
}
