package com.watch.help.helpwatch.Helpers;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class HelperFunctions {

    public static boolean isDataAvailable(String debugTag, Context context){
        ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiConn = networkInfo.isConnected();
        networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(networkInfo == null){
            return false;
        }
        else{
        boolean isMobileConn = networkInfo.isConnected();
        Log.d(debugTag, "Wifi connected: " + isWifiConn);
        Log.d(debugTag, "Mobile connected: " + isMobileConn);

        return isWifiConn || isMobileConn;
        }
    }

    public static boolean isOnline(String debugTag, Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
