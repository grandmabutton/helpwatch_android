package com.watch.help.helpwatch.CommunicationHandlers;

import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

import com.watch.help.helpwatch.DataReporting.UserDataManagerSingleton;
import com.watch.help.helpwatch.Helpers.EmergencyContactHandler;
import com.watch.help.helpwatch.Helpers.EmergencyContactRetriever;
import com.watch.help.helpwatch.PhoneDialing.PhoneDialActivity;

import static com.watch.help.helpwatch.HelpCall.CancelHelpActivity.getAlarmTime;

class CellularPhoneAndSmsHandler extends BasePhoneAndSmsHandler {

    private final Context mParentContext;
    private String[] mEmergencyContacts;
    private EmergencyContactRetriever mContactRetriever;

    CellularPhoneAndSmsHandler(Context context, String watchId){
        mParentContext = context;
        mContactRetriever = new EmergencyContactRetriever(context,watchId);
        mContactRetriever.requestEmergencyContacts();
    }

    private String[] getEmergencyContacts(){
        if(mEmergencyContacts == null) {
            EmergencyContactHandler contactHandler = new EmergencyContactHandler(mParentContext);
            mEmergencyContacts = contactHandler.retrieveEmergencyContacts();
        }
        return mEmergencyContacts;
    }

    private void textEveryone(String message){
        for (String contact : getEmergencyContacts()) {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(contact, null,
                        message,null, null);
            } catch (Exception ex) {
                Log.d("CellHandler", "Text failed to send.");
            }
        }
    }

    @Override
    public void sendEmergencyTexts() {
        String alarmtime = Integer.toString(Math.round(getAlarmTime())/10);
        textEveryone("Your friend's HelpWatch alert was activated!!  " +
                "Emergency calls will be made in " +
                alarmtime + " seconds." +
                "Text " + getEmergencyContacts()[0] + " for updates.");
    }

    @Override
    public void sendFinalTexts(){
        UserDataManagerSingleton dataManager = UserDataManagerSingleton.getInstance(mParentContext);
        double lastLat = (double)dataManager.getUserData("Latitude");
        double lastLong = (double)dataManager.getUserData("Longitude");

        if(lastLat == -1 || lastLong == -1 || (lastLat == 0 && lastLong == 0)){
            textEveryone("Latest location unknown.");
        }
        else{
            textEveryone("Last known location: https://www.google.com/maps?q="+lastLat+","+lastLong);
        }
    }

    @Override
    public void sendCancellations() {
        textEveryone("Your friend's HelpWatch alert was cancelled.");
    }

    @Override
    public void makeEmergencyCalls() {
        for (String contact : getEmergencyContacts()) {
            try {
                Intent intent = new Intent(mParentContext, PhoneDialActivity.class);
                intent.putExtra("emergencyContact", contact);
                mParentContext.startActivity(intent);
            } catch (Exception ex) {
                Log.d("CellHandler", "Call to " + contact + " failed." );
            }
        }
    }
}

