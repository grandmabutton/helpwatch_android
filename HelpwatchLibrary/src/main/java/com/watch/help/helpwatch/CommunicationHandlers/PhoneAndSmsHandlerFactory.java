package com.watch.help.helpwatch.CommunicationHandlers;

import android.content.Context;

import com.watch.help.helpwatch.Helpers.HelperFunctions;

public class PhoneAndSmsHandlerFactory {
    public BasePhoneAndSmsHandler build(String debugTag, Context context, String watchId){
        if (HelperFunctions.isOnline(debugTag,context)){
            return new TwilioPhoneAndSmsHandler(context, watchId);
        }
        else return new CellularPhoneAndSmsHandler(context, watchId);
    }
}