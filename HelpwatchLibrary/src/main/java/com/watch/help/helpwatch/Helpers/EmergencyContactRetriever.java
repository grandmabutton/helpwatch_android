package com.watch.help.helpwatch.Helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.watch.help.helpwatch.DataReporting.DataPush.DataRequestSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.watch.help.helpwatch.BuildConfig.HELPWATCH_SERVER_URL;

public class EmergencyContactRetriever {

    private final Context mParentContext;
    private final String mWatchId;
    private EmergencyContactHandler mContactHandler;

    public EmergencyContactRetriever(Context parentContext, String watchId){
        mParentContext = parentContext;
        mWatchId = watchId;
    }

    public void requestEmergencyContacts() {
        mContactHandler = new EmergencyContactHandler(mParentContext);
        StringRequest request = new StringRequest(Request.Method.POST, HELPWATCH_SERVER_URL + "/postEmergencyContacts",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = (JSONArray) jsonObject.get("emergencyContacts");
                            String[] emergencyContacts = new String[jsonArray.length()];
                            for (int i = 0; i < jsonArray.length(); i++) {
                                emergencyContacts[i] = (String) jsonArray.get(i);
                            }
                            mContactHandler.storeEmergencyContacts(emergencyContacts);
                        } catch (JSONException e) {
                            Log.d("Error", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("WatchID", mWatchId);
                return params;
            }
        };

        DataRequestSingleton.getInstance(mParentContext).addToRequestQueue(request);
    }
}