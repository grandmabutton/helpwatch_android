package com.watch.help.helpwatch.DataReporting;

import java.util.HashMap;
import java.util.Map;

class UserDataContainer {

    private Map<String,Object> mUserDataMap = new HashMap<String,Object>();

    UserDataContainer(String userId){
        mUserDataMap.put("HeartRate",-1);
        mUserDataMap.put("Latitude", (double)0);
        mUserDataMap.put("Longitude", (double)0);
        mUserDataMap.put("Acceleration",(float)-9.8);
        mUserDataMap.put("IsWorn",true);
        mUserDataMap.put("StepCount", -1);
        mUserDataMap.put("BatteryLife", -1);
        mUserDataMap.put("Identity",userId);
    }

    void updateUserData(String dataKey, Object value){
        mUserDataMap.put(dataKey,value);
    }

    Object getUserData(String dataKey){
        return mUserDataMap.get(dataKey);
    }
}