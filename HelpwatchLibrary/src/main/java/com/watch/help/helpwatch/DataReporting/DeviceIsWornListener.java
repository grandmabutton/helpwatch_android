package com.watch.help.helpwatch.DataReporting;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.TableRow;

import java.util.Objects;

public class DeviceIsWornListener implements SensorEventListener {

    private final Context mContext;
    private boolean mWasWorn;

    DeviceIsWornListener(Context context){
        mWasWorn = false;
        mContext = context.getApplicationContext();
        SensorManager mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        boolean hasSensor = Objects.requireNonNull(mSensorManager).getDefaultSensor(34,
                true /* wakeup */) != null;
        if(hasSensor){
            Sensor mIsDeviceWornSensor = mSensorManager.getDefaultSensor(34, true);
            mSensorManager.registerListener(this, mIsDeviceWornSensor,100);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int isWorn = (int)(event.values[0]);
        boolean isDeviceWorn = isWorn == 1;
        UserDataManagerSingleton dataManager = UserDataManagerSingleton.getInstance(mContext);
        if(!isDeviceWorn && mWasWorn){
            dataManager.pauseDataCollection();
            mWasWorn = false;
        }
        else if(isDeviceWorn && !mWasWorn){
            dataManager.resumeDataCollection();
            mWasWorn = true;
        }
        dataManager.updateUserData("IsWorn",isDeviceWorn);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
