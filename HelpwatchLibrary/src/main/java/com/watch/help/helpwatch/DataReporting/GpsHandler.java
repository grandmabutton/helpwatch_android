package com.watch.help.helpwatch.DataReporting;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

class GpsHandler {

    private final Context mContext;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private static final String mDebugTag = "GPS_HANDLER";

    GpsHandler(Context context) {
        mContext = context.getApplicationContext();
        mFusedLocationClient = new FusedLocationProviderClient(mContext);
        launchBackgroundGps();
    }

    @SuppressLint("MissingPermission")
    //Permissions are handled elsewhere.
    private void launchBackgroundGps(){
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(5 * 1000 * 60);
        locationRequest.setFastestInterval(1000 * 60);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                onLocationChanged(locationResult.getLastLocation());
            }
        };
        mFusedLocationClient.requestLocationUpdates(locationRequest,mLocationCallback ,
                Looper.myLooper());
        Log.d(mDebugTag,"Background GPS launched.");
    }

    @SuppressLint("MissingPermission")
    //Permissions are handled elsewhere.
    void launchEmergencyGps(){
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5 * 1000);
        locationRequest.setFastestInterval(1000);

        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                onLocationChanged(locationResult.getLastLocation());
            }
        };
        mFusedLocationClient.requestLocationUpdates(locationRequest,mLocationCallback,
                Looper.myLooper());
        Log.d(mDebugTag,"Emergency GPS started.");
    }

    void restoreBackgroundGps(){
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        launchBackgroundGps();
        Log.d(mDebugTag,"Background GPS restored.");
    }

    private void onLocationChanged(Location loc){
        UserDataManagerSingleton dataManager = UserDataManagerSingleton.getInstance(mContext);
        dataManager.updateUserData("Longitude",loc.getLongitude());
        dataManager.updateUserData("Latitude", loc.getLatitude());
        Log.d(mDebugTag,"Gps recorded.");
    }

    void pause() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        Log.d(mDebugTag,"GPS Paused.");

    }
}