package com.watch.help.helpwatch.Notifications;

import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.watch.help.helpwatch.Alerts.NotificationAlert;
import com.watch.help.helpwatch.DataReporting.DataPush.DataRequestSingleton;
import com.watch.help.helpwatch.DataReporting.UserDataManagerSingleton;
import com.watch.help.helpwatch.R;

import java.util.HashMap;
import java.util.Map;

import static android.os.PowerManager.ACQUIRE_CAUSES_WAKEUP;
import static android.os.PowerManager.FULL_WAKE_LOCK;
import static com.watch.help.helpwatch.BuildConfig.HELPWATCH_SERVER_URL;

public class NotificationActivity extends WearableActivity {

    private NotificationAlert mNotificationAlert;
    private String mWatchId;
    private String mBulkId;
    private PowerManager.WakeLock mWl;
    private static final String mDebugTag = "notification: activity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("CUSTOM", "creating notify service");

        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        assert pm != null;
        mWl = pm.newWakeLock(FULL_WAKE_LOCK | ACQUIRE_CAUSES_WAKEUP, mDebugTag);
        Log.d(mDebugTag, "Acquiring lock");
        mWl.acquire(1*60*1000L /*1 minute*/);

        Bundle b = getIntent().getExtras();
        assert b != null;
        mBulkId = b.getString("bulkId");

        mNotificationAlert = new NotificationAlert(this);
        mNotificationAlert.startAlert();

        mWatchId = (String)UserDataManagerSingleton.
                getInstance(this.getApplicationContext()).
                getUserData("Identity");

        setContentView(R.layout.activity_notification);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        FloatingActionButton approveNotificationFab = findViewById(R.id.buttonPositive);
        approveNotificationFab.setOnClickListener(approveFabClickListener());

        FloatingActionButton rejectNotificationFab = findViewById(R.id.buttonNegative);
        rejectNotificationFab.setOnClickListener(rejectFabClickListener());
        mWl.release();

    }

    private void sendNotificationResponse(final String resp) {
        StringRequest request = new StringRequest(Request.Method.POST, HELPWATCH_SERVER_URL + "/notificationCallback",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Success", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("WatchID", mWatchId);
                params.put("Body", resp);
                params.put("BulkId", mBulkId);
                return params;
            }
        };
        DataRequestSingleton.getInstance(this).addToRequestQueue(request);
    }


    private View.OnClickListener approveFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNotificationResponse("true");
                mNotificationAlert.endAlert();
                finish();
            }
        };
    }

    private View.OnClickListener rejectFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNotificationResponse("false");
                mNotificationAlert.endAlert();
                finish();
            }
        };
    }
}
