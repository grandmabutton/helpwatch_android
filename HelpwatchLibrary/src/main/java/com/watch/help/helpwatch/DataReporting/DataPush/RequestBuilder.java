package com.watch.help.helpwatch.DataReporting.DataPush;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.util.Log;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.watch.help.helpwatch.DataReporting.DefaultBatteryLifeRetriever;
import com.watch.help.helpwatch.DataReporting.UserDataManagerSingleton;
import com.watch.help.helpwatch.Helpers.HelperFunctions;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import static com.watch.help.helpwatch.BuildConfig.HELPWATCH_SERVER_URL;


class RequestBuilder {

    private static final String DEBUG_TAG = "REQUEST:BUILDER";
    private final Context mContext;
    private PowerManager.WakeLock mWl;

    RequestBuilder(Context context){
        mContext = context.getApplicationContext();
    }

    StringRequest BuildRequest(){
        PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        assert pm != null;
        mWl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, DEBUG_TAG);
        Log.d(DEBUG_TAG, "Acquiring lock");
        mWl.acquire(10*60*1000L /*10 minutes*/);

        Log.d(DEBUG_TAG, "Entering report data");
        @SuppressLint("SimpleDateFormat") //Time zone shenanigans are handled platform side.
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ssZ");
        TimeZone tz = TimeZone.getDefault();
        sdf.setTimeZone(tz);
        String timeStamp = sdf.format(Calendar.getInstance().getTime());
        Log.d(DEBUG_TAG, timeStamp);
        Log.d(DEBUG_TAG, "Network access: " + HelperFunctions.isOnline(DEBUG_TAG,
                mContext));

        UserDataManagerSingleton userDataManager = UserDataManagerSingleton.getInstance(mContext);
        DefaultBatteryLifeRetriever batteryRetriever = new DefaultBatteryLifeRetriever(mContext);
        double batteryLife = batteryRetriever.GetPercentBatteryLife();
        userDataManager.updateUserData("BatteryLife",batteryLife);
        double latitude = (double)userDataManager.getUserData("Latitude");
        double longitude = (double)userDataManager.getUserData("Longitude");
        int stepCount = (int)userDataManager.getUserData("StepCount");
        int heartRate = (int)userDataManager.getUserData("HeartRate");
        float acceleration = (float)userDataManager.getUserData("Acceleration");
        String userId = (String)userDataManager.getUserData("Identity");
        boolean isWorn = (boolean)userDataManager.getUserData("Is" +
                "Worn");
        StringRequest request = createStringRequest(userId, timeStamp, batteryLife, latitude,
                longitude, stepCount, heartRate, acceleration,isWorn);
        try {
            Log.d(DEBUG_TAG, "Post request sent.");
        } catch (Exception e) {
            Log.e(DEBUG_TAG, "Request failed.");
        }

        mWl.release();
        return request;
    }

    private StringRequest createStringRequest(final String userId,
                                              final String timeStamp,
                                              final double batteryLife,
                                              final double latitude,
                                              final double longitude,
                                              final int stepCount,
                                              final int heartRate,
                                              final double acceleration,
                                              final boolean isWorn) {

        return new StringRequest(Request.Method.POST, HELPWATCH_SERVER_URL + "/posttest",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        if (mWl.isHeld()) {
                            mWl.release();
                        }
                        Log.d("Response", "Success response: " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWl.isHeld()) {
                            mWl.release();
                        }
                        try {
                            NetworkResponse response = error.networkResponse;
                            Log.d("Error.Response", "Error" + response);
                        } catch (Exception e) {
                            Log.d("Error.Response", "Exception");
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("DateTime", timeStamp);
                params.put("BatteryLife", String.valueOf(batteryLife));
                params.put("Latitude", String.valueOf(latitude));
                params.put("Longitute", String.valueOf(longitude));
                params.put("StepCount", String.valueOf(stepCount));
                params.put("HeartRate", String.valueOf(heartRate));
                params.put("WatchID", String.valueOf(userId));
                params.put("Acceleration",String.valueOf(acceleration));
                params.put("OnWrist",String.valueOf(isWorn));
                Log.d(DEBUG_TAG,params.toString());
                return params;
            }
        };
    }
}