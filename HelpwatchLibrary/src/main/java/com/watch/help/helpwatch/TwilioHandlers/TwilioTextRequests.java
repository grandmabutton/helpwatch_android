package com.watch.help.helpwatch.TwilioHandlers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.watch.help.helpwatch.DataReporting.DataPush.DataRequestSingleton;

import java.util.HashMap;
import java.util.Map;

import static com.watch.help.helpwatch.BuildConfig.HELPWATCH_SERVER_URL;

public class TwilioTextRequests {

    private Context context;
    private String mWatchId;

    public TwilioTextRequests(Context context, String watchId) {
        this.context = context;
        this.mWatchId = watchId;
    }

    public void sendCancelledEmergencyTexts() {
        sendRequestToServer("/sendCancelledEmergencyTexts");
    }


    public void sendEmergencyTexts() {
        sendRequestToServer("/sendEmergencyTexts");
    }

    private void sendRequestToServer(String endpoint) {
        StringRequest request = new StringRequest(Request.Method.POST, HELPWATCH_SERVER_URL + endpoint,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Success", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("WatchID", mWatchId);
                return params;
            }
        };
        DataRequestSingleton.getInstance(this.context).addToRequestQueue(request);


    }
}