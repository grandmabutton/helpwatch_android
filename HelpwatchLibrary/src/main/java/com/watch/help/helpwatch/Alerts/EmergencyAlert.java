package com.watch.help.helpwatch.Alerts;

import android.content.Context;

import com.watch.help.helpwatch.R;

public class EmergencyAlert extends BaseAlert {

    public EmergencyAlert(Context context) {
        super(context);
        this.mAlarmSound = R.raw.emergency_alarm;
        this.mVibrationPattern = new long[] {600, 200, 200, 200, 100, 100, 100, 100};
    }
}
