package com.watch.help.helpwatch.Alerts;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;

import com.watch.help.helpwatch.R;

import static android.content.Context.VIBRATOR_SERVICE;

public class BaseAlert {

    private Context mContext;
    private Vibrator mVibrator;
    private MediaPlayer mPlayer;
    int mAlarmSound;
    long[] mVibrationPattern;


    BaseAlert(Context context) {
        mContext = context;
        maxVolume();
    }

    public void startAlert() {
        startAlarmSound();
        startAlarmVibrate();
    }

    public void endAlert() {
        endAlarmSound();
        endAlarmVibrate();
    }

    private void maxVolume() {
        AudioManager mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        assert mAudioManager != null;
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
    }

    private void startAlarmSound() {
        mPlayer = MediaPlayer.create(mContext, mAlarmSound);
        mPlayer.setLooping(true);
        mPlayer.start();
    }

    private void endAlarmSound() {
        mPlayer.stop();
        mPlayer.reset();
        mPlayer.release();
    }

    private void startAlarmVibrate() {
        mVibrator = (Vibrator) mContext.getSystemService(VIBRATOR_SERVICE);
        assert mVibrator != null;
        mVibrator.vibrate(mVibrationPattern, 0);
    }

    private void endAlarmVibrate() {
        mVibrator.cancel();
    }

}
