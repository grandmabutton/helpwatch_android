package com.watch.help.helpwatch.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.watch.help.helpwatch.DataReporting.UserDataManagerSingleton;

import java.util.ArrayList;
import java.util.List;

public class EmergencyContactHandler {
    private final Context mParentContext;

    public EmergencyContactHandler(Context parentContext){

        mParentContext = parentContext;
    }

    void storeEmergencyContacts(String[] contacts){
        UserDataManagerSingleton dataManager = UserDataManagerSingleton.getInstance(mParentContext);
        int index = 0;
        for (String contact : contacts) {
            dataManager.updateUserData("Contact_" + Integer.toString(index),contact);
            index++;
        }
    }

    public String[] retrieveEmergencyContacts(){
        UserDataManagerSingleton dataManager = UserDataManagerSingleton.getInstance(mParentContext);
        List<String> contacts = new ArrayList<>();

        int index = 0;
        String result;
        do{
                result = (String)dataManager.getUserData("Contact_"+Integer.toString(index));
                if(result != null) {
                    contacts.add(result);
                    index++;
                }
        }
        while(result != null);
        String[] arrayResult = new String[contacts.size()];
        arrayResult = contacts.toArray(arrayResult);
        return arrayResult;
    }
}
