package com.watch.help.helpwatch.DataReporting;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class StepCountListener implements SensorEventListener {

    private final Context mContext;
    private final static String mDebugTag = "STEP_SENSOR";
    private final SensorManager mSensorManager;
    private final Sensor mStepCounter;

    StepCountListener(Context context){
        mContext = context.getApplicationContext();
        mSensorManager = (SensorManager)mContext.getSystemService(Context.SENSOR_SERVICE);
        assert mSensorManager != null;

        mStepCounter = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        boolean isRegistered = mSensorManager.registerListener(this, mStepCounter,
                SensorManager.SENSOR_DELAY_NORMAL);
        if (!isRegistered) {
            Log.d(mDebugTag, "Registered?  !@#$%^&*NOPE!@#$%^");
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int stepCount = (int) event.values[0];
        UserDataManagerSingleton dataManager = UserDataManagerSingleton.getInstance(mContext);
        dataManager.updateUserData("StepCount",stepCount);
        Log.d(mDebugTag,"Step count updated.");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    void pause() {
        mSensorManager.unregisterListener(this,mStepCounter);
        Log.d(mDebugTag, "Sensor paused.");

    }

    void initiateNormalOperation() {
        boolean isRegistered = mSensorManager.registerListener(this, mStepCounter,
                SensorManager.SENSOR_DELAY_NORMAL);
        if (!isRegistered) {
            Log.d(mDebugTag, "Registered?  !@#$%^&*NOPE!@#$%^");
        }
    }
}
