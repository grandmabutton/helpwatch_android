package com.watch.help.helpwatch.DataReporting;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class DefaultBatteryLifeRetriever{

    private Context mCallingContext;

    public DefaultBatteryLifeRetriever(Context callingContext){
        mCallingContext = callingContext.getApplicationContext();
    }

    public float GetPercentBatteryLife(){
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = mCallingContext.registerReceiver(null, intentFilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        return level / (float)scale;
    }
}
