package com.watch.help.helpwatch.DataReporting;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class HeartRateListener implements SensorEventListener {

    private final Context mContext;
    private SensorManager mSensorManager;
    private Sensor mHeartSensor;
    private final static String mDebugTag = "HEART_SENSOR";
    private static int MICRO_SECONDS_PER_SECOND = 1000000;

    HeartRateListener(Context context){
        mContext = context.getApplicationContext();
        mSensorManager = (SensorManager)mContext.getSystemService(Context.SENSOR_SERVICE);
        assert mSensorManager != null;
        mHeartSensor = mSensorManager.getDefaultSensor(21);
        initiateNormalOperation();
    }

    void initiateEmergencyOperation(){
        mSensorManager.unregisterListener(this);
        Log.d(mDebugTag,"Begin emergency mode.");
        boolean isRegistered = mSensorManager.registerListener(this, mHeartSensor,
                SensorManager.SENSOR_DELAY_FASTEST);
        if (!isRegistered) {
            Log.d(mDebugTag, "Emergency registration? !@#$%^&*NOPE!@#$%^");
        }
    }

    void initiateNormalOperation(){
        mSensorManager.unregisterListener(this);
        Log.d(mDebugTag,"Begin normal operation.");
        boolean isRegistered = mSensorManager.registerListener(this, mHeartSensor,
                30*MICRO_SECONDS_PER_SECOND);
        if (!isRegistered) {
            Log.d(mDebugTag, "Registered? !@#$%^&*NOPE!@#$%^");
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int heartRate = (int) event.values[0];
        UserDataManagerSingleton dataManager = UserDataManagerSingleton.getInstance(mContext);
        dataManager.updateUserData("HeartRate",heartRate);
        Log.d(mDebugTag,"Heart reading taken.");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    void pause(){
        mSensorManager.unregisterListener(this, mHeartSensor);
        Log.d(mDebugTag, "Sensor paused.");
    }
}