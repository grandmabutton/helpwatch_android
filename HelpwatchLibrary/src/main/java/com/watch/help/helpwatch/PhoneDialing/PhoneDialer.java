package com.watch.help.helpwatch.PhoneDialing;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

public class PhoneDialer {

    @SuppressLint("MissingPermission")
    public void makePhoneCall(String n, Activity currentActivity) {
        char ench[] = n.toCharArray();
        String tel = "";
        for (int i = 0; i < ench.length; i++) {
            if (ench[i] == '#')
                tel = tel + Uri.encode("#");
            else
                tel = tel + ench[i];
        }
        String toDial = "tel:" + tel;// msgbox(Intent.ACTION_ALL_APPS);
        Boolean hasPermission = hasPermission(currentActivity, Manifest.permission.CALL_PHONE);
        currentActivity.startActivityForResult(
                new Intent(hasPermission(currentActivity,
                        Manifest.permission.CALL_PHONE) ? Intent.ACTION_CALL
                        : Intent.ACTION_DIAL, Uri.parse(toDial)), 1024);
    }

    public static boolean hasPermission(Context context, String permission) {
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}