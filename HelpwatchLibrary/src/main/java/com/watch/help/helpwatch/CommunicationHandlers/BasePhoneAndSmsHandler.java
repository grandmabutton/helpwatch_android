package com.watch.help.helpwatch.CommunicationHandlers;

public abstract class BasePhoneAndSmsHandler {
    public abstract void sendEmergencyTexts();
    public abstract void sendFinalTexts();
    public abstract void sendCancellations();
    public abstract void makeEmergencyCalls();
}
