package com.watch.help.helpwatch.DataReporting.DataPush;

import android.content.Context;
import android.support.annotation.NonNull;

import com.watch.help.helpwatch.Helpers.EmergencyContactHandler;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class DataPushWorkManager extends Worker {


    private final Context mContext;

    public DataPushWorkManager(Context context, WorkerParameters params) {
        super(context, params);
        mContext = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        UserDataPushHandler userDataPushHandler = new UserDataPushHandler(mContext);
        userDataPushHandler.pushUserData();

        EmergencyContactHandler emergencyContactHandler = new EmergencyContactHandler(mContext);
        emergencyContactHandler.retrieveEmergencyContacts();

        return Result.success();
    }
}
