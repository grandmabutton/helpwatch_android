package com.watch.help.helpwatch.HelpCall;

import android.Manifest;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.watch.help.helpwatch.Alerts.EmergencyAlert;
import com.watch.help.helpwatch.CommunicationHandlers.BasePhoneAndSmsHandler;
import com.watch.help.helpwatch.CommunicationHandlers.PhoneAndSmsHandlerFactory;
import com.watch.help.helpwatch.DataReporting.UserDataManagerSingleton;
import com.watch.help.helpwatch.R;


public class CancelHelpActivity extends WearableActivity {

    private static float endingTimeInSeconds = 300f;
    private boolean mCountdownActive;
    private boolean mTextSent;
    private boolean mCallMade;

    private EmergencyAlert mEmergencyAlert;

    private BasePhoneAndSmsHandler mPhoneAndSmsHandler;
    private String mWatchId;
    private UserDataManagerSingleton mDataManagerSingleton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mDataManagerSingleton = UserDataManagerSingleton.getInstance(this.getApplicationContext());
        mDataManagerSingleton.startEmergencyReporting();
        mWatchId = (String)mDataManagerSingleton.getUserData("Identity");

        setContentView(R.layout.activity_cancel_help);

        PhoneAndSmsHandlerFactory factory = new PhoneAndSmsHandlerFactory();
        mPhoneAndSmsHandler = factory.build("CancelHelpActivity",this, mWatchId);

        mCountdownActive = true;
        mTextSent = false;
        mEmergencyAlert = new EmergencyAlert(this);

        FloatingActionButton cancelEmergencyActionFab = findViewById(R.id.cancelButton);
        cancelEmergencyActionFab.setOnClickListener(cancelHelpFabClickListener());

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        mEmergencyAlert.startAlert();
        setAmbientEnabled();

        final CircularProgressBar circularProgressBar = findViewById(R.id.countdown_pb);
        circularProgressBar.setColor(ContextCompat.getColor(this, R.color.hw_purple));
        circularProgressBar.setBackgroundColor(ContextCompat.getColor(this, R.color.hw_white));

        Handler handler = new Handler();
        int delay = 50; //milliseconds
        handler.postDelayed(new ProgressUpdater(endingTimeInSeconds, circularProgressBar, handler), delay);
    }

    @Override
    protected void onDestroy() {
        mEmergencyAlert.endAlert();
        mDataManagerSingleton.cancelEmergencyReporting();
        super.onDestroy();
    }

    public static float getAlarmTime(){
        return endingTimeInSeconds;
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mPhoneAndSmsHandler.sendEmergencyTexts();
        mTextSent = true;
    }

    private class ProgressUpdater implements Runnable {
        private float mEndingTimeInSeconds;
        private Handler mHandler;
        private CircularProgressBar mProgressBar;
        private float increment = 1;
        private float progress;
        private int delayInMilliseconds = 50;

        ProgressUpdater(float endingTimeInSeconds, CircularProgressBar progressBar, Handler handler) {
            mEndingTimeInSeconds = endingTimeInSeconds;
            mProgressBar = progressBar;
            mHandler = handler;
            progress = 0f;
        }

        public void run() {
            progress += increment;
            mProgressBar.setProgress(progress * 100 / mEndingTimeInSeconds);
            if (mCountdownActive) {
                if (progress <= mEndingTimeInSeconds) {
                    mHandler.postDelayed(this, delayInMilliseconds);
                } else {
                    Log.d("RUNNING FINAL", "Making emergency calls.");
                    mPhoneAndSmsHandler.sendFinalTexts();
                    mPhoneAndSmsHandler.makeEmergencyCalls();
                    mCallMade = true;
                    finish();
                }
            }
        }
    }

    private View.OnClickListener cancelHelpFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountdownActive = false;
                if (mTextSent) {
                    mDataManagerSingleton.cancelEmergencyReporting();
                    mPhoneAndSmsHandler.sendCancellations();
                }
                finish();
            }
        };
    }
}