package com.watch.help.helpwatch.DataReporting.DataPush;

import android.content.Context;

import com.android.volley.toolbox.StringRequest;

public class UserDataPushHandler{

    private final Context mContext;

    public UserDataPushHandler(Context context){
        mContext = context;
    }

    public void pushUserData(){
        RequestBuilder builder = new RequestBuilder(mContext);
        StringRequest request = builder.BuildRequest();
        DataRequestSingleton.getInstance(mContext).addToRequestQueue(request);
    }
}
