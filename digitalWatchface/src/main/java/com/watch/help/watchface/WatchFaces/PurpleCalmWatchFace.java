package com.watch.help.watchface.WatchFaces;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;

import com.watch.help.watchface.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class PurpleCalmWatchFace {

    private Context mParentContext;

    private Paint mDefaultBackgroundPaint;
    private Paint mBatteryPaint;
    private Paint mTimeTextPaint;
    private Paint mDateTextPaint;
    private Paint mButtonTextPaint;
    private Bitmap mBackgroundBitmap;
    private Drawable mBatteryVector;

    private int mScreenHeight;
    private int mScreenWidth;
    private SimpleDateFormat mDateFormat;


    public PurpleCalmWatchFace(Context parentContext)  {
        mParentContext = parentContext;
        mDefaultBackgroundPaint = new Paint();
        mBatteryPaint = new Paint();
        mTimeTextPaint = new Paint();
        mDateTextPaint = new Paint();
        mButtonTextPaint = new Paint();

        Typeface ralewayExtraLightFont = ResourcesCompat.getFont(mParentContext, R.font.raleway_extralight);
        Typeface ralewayItalicFont = ResourcesCompat.getFont(mParentContext, R.font.raleway_italic);
        Typeface ralewayMediumFont = ResourcesCompat.getFont(mParentContext, R.font.raleway_medium);

        mDateFormat = new SimpleDateFormat("h:mm", Locale.getDefault());
        mDateFormat.setTimeZone(TimeZone.getDefault());

        mBackgroundBitmap = BitmapFactory.decodeResource(mParentContext.getResources(), R.drawable.calm_purple_background);

        mBatteryPaint.setStyle(Paint.Style.STROKE);
        mBatteryPaint.setStrokeCap(Paint.Cap.ROUND);
        mBatteryPaint.setStrokeWidth(mParentContext.getResources().getDimension(R.dimen.og_battery_linewidth));

        mBatteryVector = mParentContext.getDrawable(R.drawable.og_battery_icon);

        mTimeTextPaint.setTextAlign(Paint.Align.CENTER);
        mTimeTextPaint.setTypeface(ralewayExtraLightFont);
        mTimeTextPaint.setAntiAlias(true);
        mTimeTextPaint.setColor(ContextCompat.getColor(mParentContext, R.color.calm_time_text));
        mTimeTextPaint.setTextSize(mParentContext.getResources().getDimension(R.dimen.calm_time_text));

        mDateTextPaint.setTextAlign(Paint.Align.CENTER);
        mDateTextPaint.setTypeface(ralewayMediumFont);
        mDateTextPaint.setAntiAlias(true);
        mDateTextPaint.setColor(ContextCompat.getColor(mParentContext, R.color.calm_date_text));
        mDateTextPaint.setTextSize(mParentContext.getResources().getDimension(R.dimen.calm_date_text));

        mButtonTextPaint.setTextAlign(Paint.Align.CENTER);
        mButtonTextPaint.setTypeface(ralewayItalicFont);
        mButtonTextPaint.setAntiAlias(true);
        mButtonTextPaint.setColor(ContextCompat.getColor(mParentContext, R.color.calm_date_text));
        mButtonTextPaint.setTextSize(mParentContext.getResources().getDimension(R.dimen.calm_button_text));

        mScreenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
        mScreenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public void onSurfaceChanged(int width, int height) {
        mBackgroundBitmap = scaleBitmap(mBackgroundBitmap, width, height);
    }

    public boolean isHealthButtonTapped(int x, int y) {
        double radiusTouch = Math.sqrt(Math.pow((double)(x - mScreenWidth / 2), 2) + Math.pow((double)(y - mScreenHeight / 2), 2));
        return y >= mScreenWidth * 3 / 5 & radiusTouch < mScreenWidth * 0.925 / 2;
    }

    public void drawWatchFace(Canvas canvas, Rect bounds, float batteryLife, boolean isTouched) {
        canvas.drawBitmap(mBackgroundBitmap, 0, 0, mDefaultBackgroundPaint);

        int batteryColor = batteryLife > 0.35 ? R.color.calm_full_battery : batteryLife > 0.15 ? R.color.og_moderate_battery : R.color.og_low_battery;
        mBatteryPaint.setColor(ContextCompat.getColor(mParentContext, batteryColor));
        mBatteryVector.setColorFilter(ContextCompat.getColor(mParentContext, batteryColor), PorterDuff.Mode.SRC_ATOP);

        int buttonTextColor = isTouched ? R.color.calm_pressed_button_text : R.color.calm_default_button_text;
        mButtonTextPaint.setColor(ContextCompat.getColor(mParentContext, buttonTextColor));
        canvas.drawText("Help", bounds.width() / 2, bounds.height() * 6 / 7, mButtonTextPaint);

        mBatteryVector.setBounds(bounds.width()/2 - 15, 14, bounds.width()/2 + 15, 29);
        mBatteryVector.draw(canvas);

        String timeText = mDateFormat.format(new Date(System.currentTimeMillis()));
        canvas.drawText(timeText, bounds.width() / 2, bounds.height() / 2 + 22, mTimeTextPaint);


        Calendar calendar = Calendar.getInstance();
        String dayText = String.format("%s", calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()));
        String monthText = String.format("%.3s", calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault()));
        String dateText = String.format(Locale.getDefault(), "%s", calendar.get(Calendar.DATE));

        canvas.drawText(dayText + ", " + monthText + " " + dateText, bounds.width() / 2, bounds.height() * 2 / 9, mDateTextPaint);

        canvas.drawArc(0, 0, bounds.width(), bounds.width(), 270, batteryLife * 360, false, mBatteryPaint);

    }

    private Bitmap scaleBitmap(Bitmap bitmap, int width, int height) {

        if (bitmap.getWidth() != width) {
            return Bitmap.createScaledBitmap(bitmap, width, height, true /* filter */);
        }
        return bitmap;
    }


}
