package com.watch.help.watchface.Notifications;

import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.watch.help.helpwatch.DataReporting.DataPush.DataRequestSingleton;
import com.watch.help.helpwatch.DataReporting.UserDataManagerSingleton;
import com.watch.help.helpwatch.Notifications.NotificationActivity;

import java.util.HashMap;
import java.util.Map;

import static com.watch.help.helpwatch.BuildConfig.HELPWATCH_SERVER_URL;

public class NotifyFirebaseMessagingService extends FirebaseMessagingService {

    private String mBindingToken = "";
    private static final String TAG = "FB_Messaging_service";
    private static final String NOTIFY_TITLE_DATA_KEY = "twi_title";
    private static final String NOTIFY_BODY_DATA_KEY = "twi_body";


    @Override
    public void onMessageReceived(RemoteMessage message) {
        String from = message.getFrom();
        Map<String, String> data = message.getData();
        String title = data.get(NOTIFY_TITLE_DATA_KEY);
        String body = data.get(NOTIFY_BODY_DATA_KEY);

        Map<String, String> bodyArgs = new HashMap<String, String>();
        assert body != null;
        String[] pairs = body.split(",");
        for (String pair : pairs) {
            String[] keyValue = pair.split(":");
            bodyArgs.put(keyValue[0], keyValue[1]);
        }

        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Body: " + body);

        Intent launchCancelHelpIntent = new Intent(getApplicationContext(), NotificationActivity.class);
        launchCancelHelpIntent.putExtra("bulkId", bodyArgs.get("bulkId"));
        launchCancelHelpIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(launchCancelHelpIntent);
    }

    @Override
    public void onNewToken(String newBindingToken) {
        Log.d("NWA_onnewtoken",newBindingToken + ",  " + mBindingToken);

        super.onNewToken(newBindingToken);

        if (!newBindingToken.equals(mBindingToken)) {
            mBindingToken = newBindingToken;
            registerBinding(newBindingToken);
        }
    }

    private void registerBinding(final String bindingToken) {
        final String mWatchId = (String)UserDataManagerSingleton.
                getInstance(this.getApplicationContext()).
                getUserData("Identity");
        Log.d("NWA_registering",mWatchId);
        StringRequest request = new StringRequest(Request.Method.POST, HELPWATCH_SERVER_URL + "/registerBinding",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("CUSTOM", "amanda binds");
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("identity", mWatchId);
                params.put("address", bindingToken);
                return params;
            }
        };
        DataRequestSingleton.getInstance(this).addToRequestQueue(request);
    }

}


