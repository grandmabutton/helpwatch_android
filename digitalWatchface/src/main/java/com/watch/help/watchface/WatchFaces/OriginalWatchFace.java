package com.watch.help.watchface.WatchFaces;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.watch.help.watchface.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class OriginalWatchFace {

    private Context mParentContext;

    private Paint mDefaultBackgroundPaint;
    private Paint mHelpButtonPaint;
    private Paint mBatteryPaint;
    private Paint mTimeTextPaint;
    private Paint mDateTextPaint;
    private Bitmap mBackgroundBitmap;
    private Bitmap mHelpButtonBitmap;
    private Bitmap mPressedHelpButtonBitmap;
    private Drawable mBatteryVector;
    private int mScreenHeight;
    private int mScreenWidth;
    private SimpleDateFormat mDateFormat;


    public OriginalWatchFace(Context parentContext)  {
        mParentContext = parentContext;
        mDefaultBackgroundPaint = new Paint();
        mHelpButtonPaint = new Paint();
        mBatteryPaint = new Paint();
        mTimeTextPaint = new Paint();
        mDateTextPaint = new Paint();

        mDateFormat = new SimpleDateFormat("h:mm", Locale.getDefault());
        mDateFormat.setTimeZone(TimeZone.getDefault());


        mBackgroundBitmap = BitmapFactory.decodeResource(mParentContext.getResources(), R.drawable.og_background);
        mHelpButtonBitmap = BitmapFactory.decodeResource(mParentContext.getResources(), R.drawable.og_default_button);
        mPressedHelpButtonBitmap = BitmapFactory.decodeResource(mParentContext.getResources(), R.drawable.og_pressed_button);

        mBatteryPaint.setStyle(Paint.Style.STROKE);
        mBatteryPaint.setStrokeCap(Paint.Cap.ROUND);
        mBatteryPaint.setStrokeWidth(mParentContext.getResources().getDimension(R.dimen.og_battery_linewidth));

        mBatteryVector = mParentContext.getDrawable(R.drawable.og_battery_icon);

        mTimeTextPaint.setTextAlign(Paint.Align.CENTER);
        mTimeTextPaint.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        mTimeTextPaint.setAntiAlias(true);
        mTimeTextPaint.setColor(ContextCompat.getColor(mParentContext, R.color.og_time_text));
        mTimeTextPaint.setTextSize(mParentContext.getResources().getDimension(R.dimen.og_time_text));

        mDateTextPaint.setTextAlign(Paint.Align.CENTER);
        mDateTextPaint.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        mDateTextPaint.setAntiAlias(true);
        mDateTextPaint.setColor(ContextCompat.getColor(mParentContext, R.color.og_date_text));
        mDateTextPaint.setTextSize(mParentContext.getResources().getDimension(R.dimen.og_date_text));

        mScreenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
        mScreenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public void onSurfaceChanged(int width, int height) {
        mBackgroundBitmap = scaleBitmap(mBackgroundBitmap, width, height);
        mHelpButtonBitmap = scaleBitmap(mHelpButtonBitmap, width);
        mPressedHelpButtonBitmap = scaleBitmap(mPressedHelpButtonBitmap, width);
    }

    public boolean isHealthButtonTapped(int x, int y) {
        double radiusTouch = Math.sqrt(Math.pow((double)(x - mScreenWidth / 2), 2) + Math.pow((double)(y - mScreenHeight / 2), 2));
        return y >= mScreenWidth / 2 & radiusTouch < mScreenWidth * 0.925 / 2;
    }

    public void drawWatchFace(Canvas canvas, Rect bounds, float batteryLife, boolean isTouched) {
        canvas.drawBitmap(mBackgroundBitmap, 0, 0, mDefaultBackgroundPaint);

        int batteryColor = batteryLife > 0.35 ? R.color.og_full_battery : batteryLife > 0.15 ? R.color.og_moderate_battery : R.color.og_low_battery;
        mBatteryPaint.setColor(ContextCompat.getColor(mParentContext, batteryColor));
        mBatteryVector.setColorFilter(ContextCompat.getColor(mParentContext, batteryColor), PorterDuff.Mode.SRC_ATOP);

        if (isTouched) {
            canvas.drawBitmap(mPressedHelpButtonBitmap, 0, bounds.height() - mPressedHelpButtonBitmap.getHeight(), mHelpButtonPaint);
        } else {
            canvas.drawBitmap(mHelpButtonBitmap, 0, bounds.height() - mHelpButtonBitmap.getHeight(), mHelpButtonPaint);
        }

        mBatteryVector.setBounds(bounds.width()/2 - 15, 14, bounds.width()/2 + 15, 29);
        mBatteryVector.draw(canvas);

        String timeText = mDateFormat.format(new Date(System.currentTimeMillis()));

        canvas.drawText(timeText, bounds.width() / 2, bounds.height() / 2 + 12, mTimeTextPaint);
        Calendar calendar = Calendar.getInstance();
        String dayText = String.format("%s", calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())).toUpperCase();
        String monthText = String.format("%.3s", calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault())).toUpperCase();
        String dateText = String.format(Locale.getDefault(), "%s", calendar.get(Calendar.DATE));

        canvas.drawText(dayText + ", " + monthText + " " + dateText, bounds.width() / 2, bounds.height() * 2 / 9, mDateTextPaint);

        canvas.drawArc(0, 0, bounds.width(), bounds.width(), 270, batteryLife * 360, false, mBatteryPaint);

    }

    private Bitmap scaleBitmap(Bitmap bitmap, int width, int height) {

        if (bitmap.getWidth() != width) {
            return Bitmap.createScaledBitmap(bitmap, width, height, true /* filter */);
        }
        return bitmap;
    }

    private Bitmap scaleBitmap(Bitmap bitmap, int width) {

        if (bitmap.getWidth() != width) {
            double aspect_ratio = 1.0 * bitmap.getHeight() / bitmap.getWidth();
            return Bitmap.createScaledBitmap(bitmap, width, (int) (width * aspect_ratio), true);
        }
        return bitmap;
    }

}
