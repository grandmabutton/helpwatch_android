package com.watch.help.watchface;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.watch.help.helpwatch.DataReporting.DefaultBatteryLifeRetriever;
import com.watch.help.helpwatch.DataReporting.UserDataManagerSingleton;
import com.watch.help.helpwatch.HelpCall.CancelHelpActivity;
import com.watch.help.helpwatch.Helpers.EmergencyContactRetriever;
import com.watch.help.watchface.Notifications.NotifyFirebaseMessagingService;
//import com.watch.help.watchface.WatchFaces.BlueCalmWatchFace;
//import com.watch.help.watchface.WatchFaces.PurpleCalmWatchFace;
import com.watch.help.watchface.WatchFaces.MarbleWatchFace;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public class DigitalWatchFace extends CanvasWatchFaceService {
    /**
     * Update rate in milliseconds for interactive mode. Defaults to one second
     * because the watch face needs to update seconds in interactive mode.
     */
    private static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(30);
    private UserDataManagerSingleton mDataManager;
    private static final int MSG_UPDATE_TIME = 0;

    @Override
    public Engine onCreateEngine() {
        FirebaseApp.initializeApp(this);
        mDataManager = UserDataManagerSingleton.getInstance(this.getApplicationContext());
        String identity = (String)mDataManager.getUserData("Identity");

        EmergencyContactRetriever retriever = new EmergencyContactRetriever(this, identity);
        retriever.requestEmergencyContacts();

        Intent notificationsIntent = new Intent(this, NotifyFirebaseMessagingService.class);
        startService(notificationsIntent);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Log.d("NWA_successlisten", token);
            }
        });

        return new Engine();
    }

    private static class EngineHandler extends Handler {
        private final WeakReference<DigitalWatchFace.Engine> mWeakReference;

        EngineHandler(DigitalWatchFace.Engine reference) {
            mWeakReference = new WeakReference<>(reference);
        }

        @Override
        public void handleMessage(Message msg) {
            DigitalWatchFace.Engine engine = mWeakReference.get();
            if (engine != null) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        engine.handleUpdateTimeMessage();
                        break;
                }
            }
        }
    }

    private class Engine extends CanvasWatchFaceService.Engine {

        private final Handler mUpdateTimeHandler = new EngineHandler(this);
        private Calendar mCalendar;
        private final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            }
        };
        private boolean mRegisteredTimeZoneReceiver = false;
        private boolean mIsTouched = false;
        private DefaultBatteryLifeRetriever mBatteryLifeRetriever;
        private MarbleWatchFace mWatchFace;
        private Handler longPressHandler = new Handler();

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);

            setWatchFaceStyle(new WatchFaceStyle.Builder(DigitalWatchFace.this)
                    .setAcceptsTapEvents(true)
                    .build());

            mBatteryLifeRetriever = new DefaultBatteryLifeRetriever(getApplicationContext());
            mWatchFace = new MarbleWatchFace(getApplicationContext());
            }

        @Override
        public void onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            super.onDestroy();
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            mWatchFace.onSurfaceChanged(width, height);
            super.onSurfaceChanged(holder, format, width, height);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            mDataManager.oneTimePush();
            if (visible) {
                registerReceiver();
                invalidate();
            } else {
                unregisterReceiver();
            }
            updateTimer();
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            DigitalWatchFace.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            DigitalWatchFace.this.unregisterReceiver(mTimeZoneReceiver);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            updateTimer();
        }


        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {
            mIsTouched = false;
            final boolean onHealthButton = mWatchFace.isHealthButtonTapped(x, y);

            switch (tapType) {
                case TAP_TYPE_TOUCH:
                    mIsTouched = onHealthButton;
                    longPressHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (onHealthButton) {
                                Intent launchCancelHelpIntent = new Intent(getApplicationContext(), CancelHelpActivity.class);
                                launchCancelHelpIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                try {
                                    startActivity(launchCancelHelpIntent);
                                    mIsTouched = false;
                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }, 1200);
                    break;
                case TAP_TYPE_TOUCH_CANCEL:
                case TAP_TYPE_TAP:
                    longPressHandler.removeCallbacksAndMessages(null);
                    break;
            }
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {

            float batteryLife = mBatteryLifeRetriever.GetPercentBatteryLife();
            mDataManager.updateUserData("BatteryLife",batteryLife);
            mCalendar = Calendar.getInstance();

            if (isInAmbientMode()) {
                canvas.drawColor(Color.BLACK);
            } else {
                mWatchFace.drawWatchFace(canvas, bounds, batteryLife, mIsTouched);

            }

        }

        /**
         * Starts the {@link #mUpdateTimeHandler} timer if it should be running and isn't currently
         * or stops it if it shouldn't be running but currently is.
         */
        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        /**
         * Returns whether the {@link #mUpdateTimeHandler} timer should be running. The timer should
         * only run when we're visible and in interactive mode.
         */
        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        private void handleUpdateTimeMessage() {
            invalidate();
            if (shouldTimerBeRunning()) {
                long timeMs = System.currentTimeMillis();
                long delayMs = INTERACTIVE_UPDATE_RATE_MS
                        - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
            }
        }
    }
}
