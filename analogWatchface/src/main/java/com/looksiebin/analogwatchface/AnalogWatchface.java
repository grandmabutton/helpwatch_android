package com.looksiebin.analogwatchface;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.WindowInsets;
import android.widget.Toast;

import com.looksiebin.analogwatchface.DataGrabbing.BatteryLifeRetriever;
import com.looksiebin.analogwatchface.DataPush.DataPushAlarm;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class AnalogWatchface extends CanvasWatchFaceService {

    /**
     * Update rate in milliseconds for interactive mode. Defaults to one second
     * because the watch face needs to update seconds in interactive mode.
     */
    private static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1);

    /**
     * Handler message id for updating the time periodically in interactive mode.
     */
    private static final int MSG_UPDATE_TIME = 0;
    private DataPushAlarm dataPushAlarm = new DataPushAlarm();

    @Override
    public Engine onCreateEngine() {
        dataPushAlarm.setAlarm(this);
        return new Engine();
    }

    private static class EngineHandler extends Handler {
        private final WeakReference<AnalogWatchface.Engine> mWeakReference;

        public EngineHandler(AnalogWatchface.Engine reference) {
            mWeakReference = new WeakReference<>(reference);
        }

        @Override
        public void handleMessage(Message msg) {
            AnalogWatchface.Engine engine = mWeakReference.get();
            if (engine != null) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        engine.handleUpdateTimeMessage();
                        break;
                }
            }
        }
    }

    private class Engine extends CanvasWatchFaceService.Engine {

        private final Handler mUpdateTimeHandler = new EngineHandler(this);
        private Calendar mCalendar;
        private final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            }
        };
        private boolean mRegisteredTimeZoneReceiver = false;
        private boolean mIsTouched = false;
        private Paint mDefaultBackgroundPaint;
        private Paint mHelpButtonPaint;
        private Paint mBatteryPaint;
        private Paint mBatteryTextPaint;
        private Paint mDateTextPaint;
        private Bitmap mBackgroundBitmap;
        private Bitmap mBackgroundScaledBitmap;
        private Bitmap mHelpButtonBitmap;
        private Bitmap mHelpButtonScaledBitmap;
        private Bitmap mPressedHelpButtonBitmap;
        private Bitmap mPressedHelpButtonScaledBitmap;

        private Paint mWatchHandsPaint;
        private Bitmap mSecondHandBitmap;
        private Bitmap mScaledSecondHandBitmap;
        private Bitmap mMinuteHandBitmap;
        private Bitmap mScaledMinuteHandBitmap;
        private Bitmap mHourHandBitmap;
        private Bitmap mScaledHourHandBitmap;

        private BatteryLifeRetriever mBatteryLifeRetriever;

        private int mScreenHeight;
        private int mScreenWidth;


        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);

            setWatchFaceStyle(new WatchFaceStyle.Builder(AnalogWatchface.this)
                    .setAcceptsTapEvents(true)
                    .build());

            mCalendar = Calendar.getInstance();

            Resources resources = AnalogWatchface.this.getResources();

            // Initializes background.
            mBackgroundBitmap = BitmapFactory
                    .decodeResource(getResources(), R.drawable.analog_background);
            mDefaultBackgroundPaint = new Paint();

            mHelpButtonBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.analog_button_default);
            mPressedHelpButtonBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.analog_button_pressed);

            mHelpButtonPaint = new Paint();

            mBatteryPaint = new Paint();
            mBatteryPaint.setStyle(Paint.Style.STROKE);
            mBatteryPaint.setStrokeCap(Paint.Cap.BUTT);
            mBatteryPaint.setAntiAlias(true);
            mBatteryPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.battery_line_color));

            mBatteryTextPaint = new Paint();
            mBatteryTextPaint.setTextAlign(Paint.Align.CENTER);
            mBatteryTextPaint.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
            mBatteryTextPaint.setAntiAlias(true);
            mBatteryTextPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.battery_line_color));
            mBatteryTextPaint.setTextSize(resources.getDimension(R.dimen.digital_text_size_date));

            // Initializes Watch Face.
            mDateTextPaint = new Paint();
            mDateTextPaint.setTextAlign(Paint.Align.CENTER);
            mDateTextPaint.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
            mDateTextPaint.setAntiAlias(true);
            mDateTextPaint.setTextSize(resources.getDimension(R.dimen.digital_text_size_date));
            mDateTextPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.dark_purple));

            // Initializes Watch Hands.
            mWatchHandsPaint = new Paint();
            mWatchHandsPaint.setAntiAlias(true);
            mWatchHandsPaint.setFilterBitmap(true);

            mSecondHandBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.second_hand);
            mMinuteHandBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.minute_hand);
            mHourHandBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.hour_hand);

            mScreenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
            mScreenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;

            mBatteryLifeRetriever = new BatteryLifeRetriever();
        }

        @Override
        public void onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            super.onDestroy();
        }

        @Override
        public void onSurfaceChanged(
                SurfaceHolder holder, int format, int width, int height) {

            if (mBackgroundScaledBitmap == null
                    || mBackgroundScaledBitmap.getWidth() != width
                    || mBackgroundScaledBitmap.getHeight() != height) {
                mBackgroundScaledBitmap = Bitmap.createScaledBitmap(mBackgroundBitmap, width, height, true /* filter */);
            }
            if (mHelpButtonScaledBitmap == null
                    || mHelpButtonScaledBitmap.getWidth() != width) {
                double aspect_ratio = 1.0 * mHelpButtonBitmap.getHeight() / mHelpButtonBitmap.getWidth();
                mHelpButtonScaledBitmap = Bitmap.createScaledBitmap(mHelpButtonBitmap, width, (int) (width * aspect_ratio), true);
            }
            if (mPressedHelpButtonScaledBitmap == null
                    || mPressedHelpButtonScaledBitmap.getWidth() != width) {
                double aspect_ratio = 1.0 * mPressedHelpButtonBitmap.getHeight() / mPressedHelpButtonBitmap.getWidth();
                mPressedHelpButtonScaledBitmap = Bitmap.createScaledBitmap(mPressedHelpButtonBitmap, width, (int) (width * aspect_ratio), true);
            }
            if (mScaledSecondHandBitmap == null
                    || mScaledSecondHandBitmap.getHeight() != height * 0.8) {
                double hand_height = height * 0.8;
                double aspect_ratio = 1.0 * mSecondHandBitmap.getWidth() / mSecondHandBitmap.getHeight();
                mScaledSecondHandBitmap = Bitmap.createScaledBitmap(mSecondHandBitmap, (int) (hand_height * aspect_ratio), (int) hand_height, true);
            }
            if (mScaledMinuteHandBitmap == null
                    || mScaledMinuteHandBitmap.getHeight() != height * 0.8) {
                double hand_height = height * 0.8;
                double aspect_ratio = 1.0 * mMinuteHandBitmap.getWidth() / mMinuteHandBitmap.getHeight();
                mScaledMinuteHandBitmap = Bitmap.createScaledBitmap(mMinuteHandBitmap, (int) (hand_height * aspect_ratio), (int) hand_height, true);
            }
            if (mScaledHourHandBitmap == null
                    || mScaledHourHandBitmap.getHeight() != height * 0.8) {
                double hand_height = height * 0.8;
                double aspect_ratio = 1.0 * mHourHandBitmap.getWidth() / mHourHandBitmap.getHeight();
                mScaledHourHandBitmap = Bitmap.createScaledBitmap(mHourHandBitmap, (int) (hand_height * aspect_ratio), (int) hand_height, true);
            }

            super.onSurfaceChanged(holder, format, width, height);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                registerReceiver();

                // Update time zone in case it changed while we weren't visible.
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            } else {
                unregisterReceiver();
            }

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            updateTimer();
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            AnalogWatchface.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            AnalogWatchface.this.unregisterReceiver(mTimeZoneReceiver);
        }


        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            updateTimer();
        }

        /**
         * Captures tap event (and tap type) and toggles the background color if the user finishes
         * a tap.
         */
        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {
            mIsTouched = false;
            boolean onHealthButton = tapOnHealthButton(x, y);

            switch (tapType) {
                case TAP_TYPE_TOUCH:
                    mIsTouched = onHealthButton;
                    // The user has started touching the screen.
                    break;
                case TAP_TYPE_TOUCH_CANCEL:
                    // The user has started a different gesture or otherwise cancelled the tap.
                    break;
                case TAP_TYPE_TAP:
                    // The user has completed the tap gesture.
                    mIsTouched = onHealthButton;
                    if (onHealthButton) {
                        mIsTouched = true;
                        ComponentName component = new ComponentName("com.watch.help.helpwatch", "com.watch.help.helpwatch.HelpCall.CancelHelpActivity");
                        Intent intent = new Intent()
                                .setComponent(component)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        try {
                            startActivity(intent);
                            mIsTouched = false;
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                    break;
            }
            invalidate();
        }

        private boolean tapOnHealthButton(int x, int y) {
            return y >= mScreenWidth / 2;
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {

            long now = System.currentTimeMillis();
            mCalendar.setTimeInMillis(now);

            // Draw the background
            canvas.drawBitmap(mBackgroundScaledBitmap, 0, 0, mDefaultBackgroundPaint);

            // Draw the button depending on if it's pressed or not.
            if (mIsTouched) {
                canvas.drawBitmap(mPressedHelpButtonScaledBitmap, 0, bounds.height() - mPressedHelpButtonScaledBitmap.getHeight(), mHelpButtonPaint);
            } else {
                canvas.drawBitmap(mHelpButtonScaledBitmap, 0, bounds.height() - mHelpButtonScaledBitmap.getHeight(), mHelpButtonPaint);
            }

            // Draw the day and date in the insets of the watch face.
            String dayText = String.format("%.3s", mCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())).toUpperCase();
            String dateText = String.format(Locale.getDefault(), "%s", mCalendar.get(Calendar.DATE));

            canvas.drawText(dayText, bounds.width() * 0.725f, (bounds.height() / 2) + 7.5f, mDateTextPaint);
            canvas.drawText(dateText, bounds.width() * 0.87f, (bounds.height() / 2) + 7.5f, mDateTextPaint);

            // Draw the battery indicator
            drawBatteryIndicator(canvas, bounds);

            // Draw the hands
            drawHands(canvas);
        }

        private void drawBatteryIndicator(Canvas canvas, Rect bounds) {
            float batteryLife = mBatteryLifeRetriever.GetBatteryLife(getApplicationContext());
            float[] batteryIndicatorBounds = getBatteryIndicatorBounds(bounds);

            if (batteryLife > 0.35) {
                mBatteryPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.digital_battery_full));
                mBatteryTextPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.digital_battery_full));

            } else if (batteryLife > 0.15) {
                mBatteryPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.digital_battery_moderate));
                mBatteryTextPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.digital_battery_moderate));

            } else {
                mBatteryPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                mBatteryPaint.setStyle(Paint.Style.FILL);
                canvas.drawCircle((batteryIndicatorBounds[0] + batteryIndicatorBounds[1]) / 2,
                        (batteryIndicatorBounds[2] + batteryIndicatorBounds[3]) / 2,
                        (batteryIndicatorBounds[3] - batteryIndicatorBounds[2]) / 2,
                        mBatteryPaint);

                mBatteryPaint.setStyle(Paint.Style.STROKE);

                mBatteryTextPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.digital_battery_low));
                mBatteryPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.digital_battery_low));

            }

            mBatteryPaint.setStrokeWidth(5);
            canvas.drawArc(batteryIndicatorBounds[0], batteryIndicatorBounds[2], batteryIndicatorBounds[1], batteryIndicatorBounds[3], 270,
                    batteryLife * 360, false, mBatteryPaint);

            mBatteryPaint.setStrokeWidth(2);
            mBatteryPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.battery_grey));
            canvas.drawArc(batteryIndicatorBounds[0], batteryIndicatorBounds[2], batteryIndicatorBounds[1], batteryIndicatorBounds[3], 270,
                    (batteryLife * 360) - 360, false, mBatteryPaint);

            canvas.drawText(String.valueOf((int) (batteryLife * 100)) + "%",
                    (batteryIndicatorBounds[0] + batteryIndicatorBounds[1]) / 2,
                    (bounds.height() / 2) + 7.5f,
                    mBatteryTextPaint);

        }

        private void drawHands(Canvas canvas){
            canvas.save();

            float secondsRotation = (mCalendar.get(Calendar.SECOND) + mCalendar.get(Calendar.MILLISECOND) / 1000f) * 6f;
            float minutesRotation = mCalendar.get(Calendar.MINUTE) * 6f;
            float hourHandOffset = mCalendar.get(Calendar.MINUTE) / 2f;
            float hoursRotation = (mCalendar.get(Calendar.HOUR) * 30) + hourHandOffset;

            int hourCenterX = (mScreenWidth - mScaledHourHandBitmap.getWidth()) / 2;
            int hourCenterY = (mScreenWidth - mScaledHourHandBitmap.getHeight()) / 2;

            canvas.rotate(hoursRotation - minutesRotation, mScreenWidth / 2, mScreenHeight / 2);
            canvas.drawBitmap(mScaledHourHandBitmap, hourCenterX, hourCenterY, mWatchHandsPaint);

            int minuteCenterX = (canvas.getWidth() - mScaledMinuteHandBitmap.getWidth()) / 2;
            int minuteCenterY = (canvas.getHeight() - mScaledMinuteHandBitmap.getHeight()) / 2;

            canvas.rotate(minutesRotation, mScreenWidth / 2, mScreenHeight / 2);
            canvas.drawBitmap(mScaledMinuteHandBitmap, minuteCenterX, minuteCenterY, mWatchHandsPaint);

            int secondCenterX = (canvas.getWidth() - mScaledSecondHandBitmap.getWidth()) / 2;
            int secondCenterY = (canvas.getHeight() - mScaledSecondHandBitmap.getHeight()) / 2;

            canvas.rotate(secondsRotation - minutesRotation, mScreenWidth / 2, mScreenHeight / 2);
            canvas.drawBitmap(mScaledSecondHandBitmap, secondCenterX, secondCenterY, mWatchHandsPaint);

            canvas.restore();

        }

        /**
         * Starts the {@link #mUpdateTimeHandler} timer if it should be running and isn't currently
         * or stops it if it shouldn't be running but currently is.
         */
        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        /**
         * Returns whether the {@link #mUpdateTimeHandler} timer should be running. The timer should
         * only run when we're visible and in interactive mode.
         */
        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        private float[] getBatteryIndicatorBounds(Rect bounds) {
            float[] batteryIndicatorBounds = new float[4];
            batteryIndicatorBounds[0] = (bounds.width() * 0.4f) - (bounds.width() * 0.25f);
            batteryIndicatorBounds[1] = (bounds.width() * 0.6f) - (bounds.width() * 0.25f);
            batteryIndicatorBounds[2] = bounds.height() * 0.4f;
            batteryIndicatorBounds[3] = bounds.height() * 0.6f;
            return batteryIndicatorBounds;
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        private void handleUpdateTimeMessage() {
            invalidate();
            if (shouldTimerBeRunning()) {
                long timeMs = System.currentTimeMillis();
                long delayMs = INTERACTIVE_UPDATE_RATE_MS
                        - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
            }
        }
    }
}
