package com.looksiebin.analogwatchface.DataPush;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.looksiebin.analogwatchface.DataGrabbing.BatteryLifeRetriever;


public class DataPushAlarm extends BroadcastReceiver
{
    @Override
    public void onReceive(final Context context, Intent intent)
    {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        // Put here YOUR code.
        //Toast.makeText(context, "Alarm !!!!!!!!!!", Toast.LENGTH_LONG).show(); // For example
        /*ComponentName component = new ComponentName("com.watch.help.helpwatch", "com.watch.help.helpwatch.HelpCall.CancelHelpActivity");
        Intent pushDataIntent = new Intent()
                .setComponent(component)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(pushDataIntent);*/
        final String url = "http://httpbin.org/get?param1=hello";

        // This is temporary
        BatteryLifeRetriever batteryLifeRetriever = new BatteryLifeRetriever();
        float batterylife = batteryLifeRetriever.GetBatteryLife(context);
        Toast.makeText(context, "Battery Life: " + batterylife, Toast.LENGTH_LONG).show();

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //Toast.makeText(context, "Response is: "+ response.substring(0,200));
                        Toast.makeText(context, "Response is: "+ response.substring(0,200), Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        DataRequestSingleton.getInstance(context).addToRequestQueue(request);

        wl.release();
    }

    public void setAlarm(Context context)
    {
        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, DataPushAlarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 30 * 1, pi); // Millisec * Second * Minute
    }

    public void cancelAlarm(Context context)
    {
        Intent intent = new Intent(context, DataPushAlarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}